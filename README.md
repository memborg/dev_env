# Developer Environment

I want to create a developer environment based on Docker

I want support for:

- Latest Vim
- Rust
- Node
- Python
- OpenJDK
- Git


## Resources

- https://github.com/GopherJ/vim-docker-env
- https://ranvir.tech/posts/rust-lang-on-alpine/
- https://www.docker.com/blog/how-to-use-the-alpine-docker-official-image/
- https://www.linuxshelltips.com/install-nodejs-alpine-linux/
- https://stackoverflow.com/questions/60014845/how-to-install-oracle-jdk11-in-alpine-linux-docker-image